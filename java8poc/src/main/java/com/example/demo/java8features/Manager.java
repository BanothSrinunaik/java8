package com.java8features;
// define interface
interface Employee1{
	
	void EmployeeInfo1();
	
	default void employeeInfo2() {
		System.out.println("Default method in interface");
	
	}
	static void employeeInfo3() {
		System.out.println("static method in interface");
		
	}
}
class Manager1 implements Employee1 {

	@Override
	public void EmployeeInfo1() {
		System.out.println("abstract method in interface");
			}
	}
		public class Manager {

			public static void main(String[] args) {
				Manager1 m=new Manager1();
				m.EmployeeInfo1();
				m.employeeInfo2();
				Employee1.employeeInfo3();
				
			}
}
