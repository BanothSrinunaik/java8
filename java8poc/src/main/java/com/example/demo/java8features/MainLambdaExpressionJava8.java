package com.java8features;

import java.util.ArrayList;

public class MainLambdaExpressionJava8 {

	public static void main(String[] args) {
		ArrayList<Object> arrayList=new ArrayList<>();
		arrayList.add(1);
		arrayList.add("SRINUNAIK");
		arrayList.add(70000.00);
		
		arrayList.forEach(x->System.out.println(x));	
	}

}

// Using lambda expression addition of two numbers.

 interface Inter 
 {
	 public void add(int a, int b);
 }
 class Test
 {
	 public static void main(String args[])
	 {
		 Inter i=(a,b)->System.out.println(+(a+b));
		 i.add(10,20);
		 i.add(200, 300);
		 
	 } 
 }
 
// Using lambda expression used by strings
 interface Interf
 {
	 public int getlength(String s);
 }
 class test1
 {
	 public static void main(String args[])
	 {
		 Interf i=s->s.length();
		 System.out.println(i.getlength("Hello World"));
		 System.out.println(i.getlength("with lambda expression"));
		 
	 }
 }
 