package com.StringNewAPI;

class sample {
	String str= null;
}

public class StringNewAPI {

	public static void main(String[] args) {
//UseCase1 isBlank()
		//java11
		//Returns if the string is empty or contains only white space
		System.out.println(" ".isBlank());
//UseCase2: strip()
		//java11
		// Returns a string whose value is this string, with all leading and trailing
		// white space removed.
		System.out.println("LR".strip());
//Usecase3: stripLeading()
		//java11
		// Returns a string whose value is this string, with all leading white space
		//removed.
		System.out.println("LR".stripLeading());
// Usecase4:stripTrailing()
		//java11
		// a string whose value is this string, with all trailing white space
		System.out.println("LR".stripTrailing());
// Usecase5:lines()
		//java11
		// Returns a stream of lines extracted from this string, separated by line
		"Line\nLine1\nLine2\nLine3\nLine4\nLine5".lines().forEach(System.out::println);
//UseCase6: transform(write your own function)
		//java12
		// This method allows the application of a function to string. The function
		//should expect a single String argument
		// and produce an result.
		System.out.println("SRINUNAIK".transform(x->x.substring(4)));
//UseCase7:	formatted(arg1,arg2)
		//java12
		// Formats using this string as the format string, and supplied arguments.
		System.out.println("My name is %s and My age is %d".formatted("Srinunaik",27));
		sample sam=new sample();
		System.out.println(sam.str.isBlank());
		


		
	}

}
